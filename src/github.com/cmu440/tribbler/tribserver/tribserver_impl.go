package tribserver

import (
	//"errors"
	"fmt"
	"encoding/json"
	"github.com/cmu440/tribbler/libstore"
	"github.com/cmu440/tribbler/rpc/storagerpc"
	"github.com/cmu440/tribbler/rpc/tribrpc"
	"github.com/cmu440/tribbler/util"
	"net"
	"net/http"
	"net/rpc"
	"sort"
	"strings"
	"time"
)

type tribServer struct {
	libstore libstore.Libstore
}

// NewTribServer creates, starts and returns a new TribServer. masterServerHostPort
// is the master storage server's host:port and port is this port number on which
// the TribServer should listen. A non-nil error should be returned if the TribServer
// could not be started.
//
// For hints on how to properly setup RPC, see the rpc/tribrpc package.
func NewTribServer(masterServerHostPort, myHostPort string) (TribServer, error) {
	tribServer := new(tribServer)
	ls, err := libstore.NewLibstore(masterServerHostPort, myHostPort, libstore.Never)
	tribServer.libstore = ls

	listener, err := net.Listen("tcp", myHostPort)
	if err != nil {
		return nil, err
	}
	// Wrap the tribServer before registering it for RPC.
	err = rpc.RegisterName("TribServer", tribrpc.Wrap(tribServer))
	if err != nil {
		return nil, err
	}
	// Setup the HTTP handler that will server incoming RPCs and
	// serve requests in a background goroutine.
	rpc.HandleHTTP()
	go http.Serve(listener, nil)
	return tribServer, nil
}

// Tia
func (ts *tribServer) CreateUser(args *tribrpc.CreateUserArgs, reply *tribrpc.CreateUserReply) error {
	value := args.UserID
	key := util.FormatUserKey(value)
	_, err := ts.libstore.Get(key)
	if err == nil {
		reply.Status = tribrpc.Exists
		return nil
	} else if err.Error() == string(storagerpc.WrongServer) {
		// checkpoint: do nothing
		return nil
	} else if err.Error() == string(storagerpc.KeyNotFound) {
		ts.libstore.Put(key, value)
		reply.Status = tribrpc.OK
		return nil
	}
	return err
}

// Tia
func (ts *tribServer) AddSubscription(args *tribrpc.SubscriptionArgs, reply *tribrpc.SubscriptionReply) error {
	keyUser := util.FormatUserKey(args.UserID)
	keyTargetUser := util.FormatUserKey(args.TargetUserID)
	_, errUser := ts.libstore.Get(keyUser)
	_, errTargetUser := ts.libstore.Get(keyTargetUser)

	if errUser == nil && errTargetUser == nil {
		key := util.FormatSubListKey(args.UserID)
		err := ts.libstore.AppendToList(key, args.TargetUserID)
		if err == nil {
			reply.Status = tribrpc.OK
		} else if err.Error() == string(storagerpc.ItemExists) {
			reply.Status = tribrpc.Exists
		}

	} else if errUser != nil && errUser.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.NoSuchUser
	} else if errTargetUser != nil && errTargetUser.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.NoSuchTargetUser
	}

	return nil
}

// Tia
func (ts *tribServer) RemoveSubscription(args *tribrpc.SubscriptionArgs, reply *tribrpc.SubscriptionReply) error {
	key := util.FormatSubListKey(args.UserID)
	err := ts.libstore.RemoveFromList(key, args.TargetUserID)
	if err == nil {
		reply.Status = tribrpc.OK
		return nil
	} else if err.Error() == string(storagerpc.WrongServer) {
		// checkpoint: do nothing
		return nil
	} else if err.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.NoSuchUser
		return nil
	} else if err.Error() == string(storagerpc.ItemNotFound) {
		reply.Status = tribrpc.NoSuchTargetUser
		return nil
	}
	return err
}

// Sophie
func (ts *tribServer) GetFriends(args *tribrpc.GetFriendsArgs, reply *tribrpc.GetFriendsReply) error {
	// reply: UserIDs, Status(OK, NoSuchUser)

	userID := util.FormatUserKey(args.UserID)
	_, err := ts.libstore.Get(userID)
	if err != nil && err.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.NoSuchUser
		return nil
	} else if err != nil {
		// unexpected error
		return err
	}

	subListKey := util.FormatSubListKey(args.UserID)
	subList, err := ts.libstore.GetList(subListKey)
	if err == nil {
		reply.Status = tribrpc.OK
		var friendIDs []string = nil
		for _, subUserID := range subList {
			subSubListKey := util.FormatSubListKey(subUserID)
			subSubList, subErr := ts.libstore.GetList(subSubListKey)
			if subErr == nil {
				friend := false
				for _, subSubUserID := range subSubList {
					if args.UserID == subSubUserID {
						friend = true
						break
					}
				}
				if friend {
					friendIDs = append(friendIDs, subUserID)
				}
			} else if subErr.Error() == string(storagerpc.KeyNotFound) {
				// subscribed user has no subcriptions
			} else if subErr.Error() == string(storagerpc.WrongServer) {
				// checkpoint: do nothing
			} else {
				// unexpected error
				return err
			}
		}
		reply.UserIDs = friendIDs
	} else if err.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.OK // no subscriptions
	} else if err.Error() == string(storagerpc.WrongServer) {
		// checkpoint: do nothing
	}
	return nil
}

// Sophie
func (ts *tribServer) PostTribble(args *tribrpc.PostTribbleArgs, reply *tribrpc.PostTribbleReply) error {
	// reply: PostKey, Status(OK, NoSuchUser)
	postTime := time.Now()

	tribListKey := util.FormatTribListKey(args.UserID)
	tribbleID := util.FormatPostKey(args.UserID, postTime.UnixNano()*int64(time.Millisecond))
	reply.PostKey = tribbleID

	userID := util.FormatUserKey(args.UserID)
	_, err := ts.libstore.Get(userID)
	if err != nil && err.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.NoSuchUser
		return nil
	} else if err != nil {
		// unexpected error
		return err
	}

	appendErr := ts.libstore.AppendToList(tribListKey, tribbleID)
	if appendErr == nil {
		tribble := &tribrpc.Tribble{UserID: args.UserID, Posted: postTime, Contents: args.Contents}
		marshalledTribble, err := json.Marshal(*tribble)
		if err != nil {
			// marshalling error
			return err
		}

		putErr := ts.libstore.Put(tribbleID, string(marshalledTribble))
		if putErr == nil {
			reply.Status = tribrpc.OK
		} else if putErr.Error() == string(storagerpc.WrongServer) {
			// checkpoint: do nothing
		} else {
			// unexpected error
			return putErr
		}
	} else if appendErr.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.NoSuchUser
	} else if appendErr.Error() == string(storagerpc.WrongServer) {
		// checkpoint: do nothing
	} else {
		// unexpected error
		return appendErr
	}

	return nil
}

// Sophie
func (ts *tribServer) DeleteTribble(args *tribrpc.DeleteTribbleArgs, reply *tribrpc.DeleteTribbleReply) error {
	// reply: Status(OK, NoSuchPost)
	userKey := util.FormatUserKey(args.UserID)
	_, userErr := ts.libstore.Get(userKey)
	if userErr != nil {
		if userErr.Error() == string(storagerpc.KeyNotFound) {
			reply.Status = tribrpc.NoSuchUser
			return nil
		}
	}
	tribListKey := util.FormatTribListKey(args.UserID)
	tribbleID := args.PostKey
	removeErr := ts.libstore.RemoveFromList(tribListKey, tribbleID)
	if removeErr == nil {
		err := ts.libstore.Delete(tribbleID)
		if err == nil {
			reply.Status = tribrpc.OK
		} else if err.Error() == string(storagerpc.WrongServer) {
			// checkpoint: do nothing
		} else {
			// unexpected error, including KeyNotFound
			return err
		}
	} else if removeErr.Error() == string(storagerpc.ItemNotFound) {
		reply.Status = tribrpc.NoSuchPost
	}

	return nil
}

// Sophie
func (ts *tribServer) GetTribbles(args *tribrpc.GetTribblesArgs, reply *tribrpc.GetTribblesReply) error {
	// reply: Tribbles, Status(OK, NoSuchUser)
	tribListKey := util.FormatTribListKey(args.UserID)
	userKey := util.FormatUserKey(args.UserID)
	_, userErr := ts.libstore.Get(userKey)
	if userErr != nil {
		if userErr.Error() == string(storagerpc.KeyNotFound) {
			reply.Status = tribrpc.NoSuchUser
			return nil
		}
	}
	tribbleIDs, getListErr := ts.libstore.GetList(tribListKey)

	userID := util.FormatUserKey(args.UserID)
	_, err := ts.libstore.Get(userID)
	if err != nil && err.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.NoSuchUser
		return nil
	} else if err != nil {
		// unexpected error
		return err
	}

	if getListErr == nil {
		reply.Status = tribrpc.OK
		var tribbles []tribrpc.Tribble = nil
		numTribbles := 100
		if len(tribbleIDs) < 100 {
			numTribbles = len(tribbleIDs)
		}

		for i := len(tribbleIDs) - 1; i >= len(tribbleIDs)-numTribbles; i-- {
			tribbleID := tribbleIDs[i]
			//for _, tribbleID := range tribbleIDs[len(tribbleIDs)-numTribbles:] {
			marshalledTribble, getErr := ts.libstore.Get(tribbleID)
			var tribble tribrpc.Tribble
			err := json.Unmarshal([]byte(marshalledTribble), &tribble)
			if err != nil {
				// error unmarshalling
				return err
			}

			if getErr == nil {
				tribbles = append(tribbles, tribble)
			} else if getErr.Error() == string(storagerpc.WrongServer) {
				// checkpoint: do nothing
			} else {
				// unexpected error; includes KeyNotFound
				return getErr
			}
		}
		reply.Tribbles = tribbles
	} else if getListErr.Error() == string(storagerpc.KeyNotFound) {

		reply.Status = tribrpc.OK // just reply with an empty list
	} else if getListErr.Error() == string(storagerpc.WrongServer) {
		// checkpoint: do nothing
	} else {
		// unexpected error
		return nil
	}

	return nil
}

// ByTime implements sort.Interface for []string based on the time
type ByTime []string

func (a ByTime) Len() int      { return len(a) }
func (a ByTime) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByTime) Less(i, j int) bool {
	i_time := strings.Split(a[i], "_")[1]
	j_time := strings.Split(a[j], "_")[1]
	return i_time < j_time
}

// Tia
func (ts *tribServer) GetTribblesBySubscription(args *tribrpc.GetTribblesArgs, reply *tribrpc.GetTribblesReply) error {
	userKey := util.FormatUserKey(args.UserID)
	_, userErr := ts.libstore.Get(userKey)
	if userErr != nil {
		if userErr.Error() == string(storagerpc.KeyNotFound) {
			reply.Status = tribrpc.NoSuchUser
			return nil
		}
	}
	subListKey := util.FormatSubListKey(args.UserID)
	subListVal, err := ts.libstore.GetList(subListKey)

	if err == nil {
		reply.Status = tribrpc.OK
		var alltribbleKeys []string //An array holding each followed users posts

		// all of the users subscribed to
		for _, sub := range subListVal {
			tribbleKeys, _ := ts.libstore.GetList(util.FormatTribListKey(sub))
			numTribbles := 100
			if len(tribbleKeys) < 100 {
				numTribbles = len(tribbleKeys)
			}
			alltribbleKeys = append(alltribbleKeys, tribbleKeys[len(tribbleKeys)-numTribbles:]...)
		}

		// sort all tribble keys in consideration (at most 100 of the most recent from each subscribed user)
		sort.Sort(ByTime(alltribbleKeys))

		numAllTribbles := 100
		if len(alltribbleKeys) < 100 {
			numAllTribbles = len(alltribbleKeys)
		}
		var tribbles []tribrpc.Tribble = nil
		for i := len(alltribbleKeys) - 1; i >= len(alltribbleKeys)-numAllTribbles; i-- {
			tribbleID := alltribbleKeys[i]
			marshalledTribble, getErr := ts.libstore.Get(tribbleID)
			var tribble tribrpc.Tribble
			
			err := json.Unmarshal([]byte(marshalledTribble), &tribble)
			fmt.Printf("tribbles:%v\n", tribble)
			if err != nil {
				// error unmarshalling
				return err
			}

			if getErr == nil {
				tribbles = append(tribbles, tribble)
			} else if getErr.Error() == string(storagerpc.WrongServer) {
				// checkpoint: do nothing
			} else {
				// unexpected error; includes KeyNotFound
				return getErr
			}
		}
		reply.Tribbles = tribbles
		return nil
	} else if err.Error() == string(storagerpc.KeyNotFound) {
		reply.Status = tribrpc.OK
		return nil
	} else {
		return err
	}

}
