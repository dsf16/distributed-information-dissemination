package libstore

import (
	"errors"
	"fmt"
	"github.com/cmu440/tribbler/rpc/storagerpc"
	"github.com/cmu440/tribbler/rpc/librpc"
	"net/rpc"
	"time"
	"sort"
	"strings"
)

const (
	Retry_times = 5
	Retry_sleep = 1 * time.Second
)

type libstore struct {
	// TODO: implement this!
	nodeList NodeList
	clientList map[string]*rpc.Client
	myHostPort string
	cache map[string]*cache_elem
	mode LeaseMode

	addToCache chan *cache_elem
	goingToRevoke chan string
	revokeKey chan string
	gointToRevokeStatus chan bool

	stalenessTable map[string]time.Time
	queryTimesTable map[string]int

	queryCache chan string
	queryCacheReply chan bool
	getCache chan string
	getCacheReply chan interface{}
	wantCache chan string
	wantCacheReply chan bool

	getConn chan string
	getConnReply chan *rpc.Client

}

type cache_elem struct {
	ValidSeconds int
	queryCacheThresh int
	isCache bool
	Key string
	Value interface{} //value is a string for get; []string for getList
}


// NewLibstore creates a new instance of a TribServer's libstore. masterServerHostPort
// is the master storage server's host:port. myHostPort is this Libstore's host:port
// (i.e. the callback address that the storage servers should use to send back
// notifications when leases are revoked).
//
// The mode argument is a debugging flag that determines how the Libstore should
// request/handle leases. If mode is Never, then the Libstore should never request
// leases from the storage server (i.e. the GetArgs.WantLease field should always
// be set to false). If mode is Always, then the Libstore should always request
// leases from the storage server (i.e. the GetArgs.WantLease field should always
// be set to true). If mode is Normal, then the Libstore should make its own
// decisions on whether or not a lease should be requested from the storage server,
// based on the requirements specified in the project PDF handout.  Note that the
// value of the mode flag may also determine whether or not the Libstore should
// register to receive RPCs from the storage servers.
//
// To register the Libstore to receive RPCs from the storage servers, the following
// line of code should suffice:
//
//     rpc.RegisterName("LeaseCallbacks", librpc.Wrap(libstore))
//
// Note that unlike in the NewTribServer and NewStorageServer functions, there is no
// need to create a brand new HTTP handler to serve the requests (the Libstore may
// simply reuse the TribServer's HTTP handler since the two run in the same process).
func NewLibstore(masterServerHostPort, myHostPort string, mode LeaseMode) (Libstore, error) {

	ls := new(libstore)
	ls.clientList = make(map[string]*rpc.Client)
	ls.cache = make(map[string]*cache_elem)
	ls.myHostPort = myHostPort
	ls.mode = mode
	ls.stalenessTable = make(map[string]time.Time)
	ls.queryTimesTable = make(map[string]int)

	ls.addToCache = make(chan *cache_elem)
	ls.goingToRevoke = make(chan string)
	ls.revokeKey = make(chan string)
	ls.gointToRevokeStatus = make(chan bool)

	ls.queryCache = make(chan string)
	ls.queryCacheReply = make(chan bool)
	ls.getCache = make(chan string)
	ls.getCacheReply = make(chan interface{})
	ls.wantCache = make(chan string)
	ls.wantCacheReply = make(chan bool)

	ls.getConn = make(chan string)
	ls.getConnReply = make(chan *rpc.Client)


	cli, err := rpc.DialHTTP("tcp", masterServerHostPort)
	if err != nil {
		return nil, err
	}

	args := storagerpc.GetServersArgs{}
	var reply storagerpc.GetServersReply

	tick := time.NewTicker(Retry_sleep)

	for i:=0; i<Retry_times; i++{
		if err := cli.Call("StorageServer.GetServers", args, &reply); err != nil {
			return nil, err
		} 
		if reply.Status == storagerpc.OK{
			ls.clientList[masterServerHostPort] = cli
			ls.nodeList = reply.Servers
			sort.Sort(ls.nodeList)
			go runServer(ls)
			rpc.RegisterName("LeaseCallbacks", librpc.Wrap(ls))
			fmt.Printf("node list is :%v\n", ls.nodeList)
			return ls, nil
		} 
		<- tick.C
	}
	
	return nil, errors.New("Fail to establish connection")
}

func (ls *libstore) getServer(key string) (*rpc.Client, error) {
	hashKey := StoreHash(strings.Split(key, ":")[0])
	var cli *rpc.Client
	for i, node := range(ls.nodeList) {
		if node.NodeID >= hashKey {
			ls.getConn <- node.HostPort
			cli = <- ls.getConnReply
			return cli, nil
		}
		if i == 0 {
			ls.getConn <- node.HostPort
			cli = <- ls.getConnReply
		}
	}

	return cli, nil
}

func runServer (ls *libstore) (error) {
	for {
		select {
		case key := <- ls.queryCache:
			_, ok := ls.cache[key]
			if ok {
				ls.queryCacheReply <- true
			} else {
				ls.queryCacheReply <- false
			}
		case key := <- ls.getCache:
			v, _ := ls.cache[key]
			ls.getCacheReply <- v
		case hostport := <- ls.getConn:
			if cli, ok := ls.clientList[hostport]; ok{
				ls.getConnReply <- cli
			} else {
				cli, err := rpc.DialHTTP("tcp", hostport)
				if err != nil {
					fmt.Printf("fail to dial \n")
				}
				ls.clientList[hostport] = cli
				ls.getConnReply <- cli
			}
		case elem := <- ls.addToCache:
			ls.cache[elem.Key] = elem
			go ls.fireLease(elem)
		case key := <- ls.goingToRevoke:
			_, ok := ls.cache[key]
			ls.gointToRevokeStatus <- ok
		case key := <- ls.revokeKey:
			delete(ls.cache, key)
		case key := <- ls.wantCache:
			num_hits, ok1 := ls.queryTimesTable[key]
			last_hit, ok2 := ls.stalenessTable[key]
			now := time.Now()

			if !ok1 || !ok2 {
				if !ok1 {
					ls.queryTimesTable[key] = 1
				}
				if !ok2 {
					ls.stalenessTable[key] = now
				}
				ls.wantCacheReply <- false

			} else {				
				num_hits += 1
				interval := now.Sub(last_hit).Seconds()


				if (interval < storagerpc.QueryCacheSeconds && num_hits >= storagerpc.QueryCacheThresh) {
					ls.wantCacheReply <- true
				} else {
					ls.queryTimesTable[key] = num_hits
					ls.stalenessTable[key] = now
					ls.wantCacheReply <- false
				}

			}

		}
	}
}

func (ls *libstore) fireLease (elem *cache_elem) {
	tick := time.Tick(time.Duration(elem.ValidSeconds) * time.Second)

	for {
		select{
		case <- tick:
			ls.revokeKey <- elem.Key
			return
		}
	}
}

func (ls *libstore) Get(key string) (string, error) {
	ls.queryCache <- key
	ok := <- ls.queryCacheReply
	if ok {
		ls.getCache <- key
		v := <- ls.getCacheReply 
		return v.(*cache_elem).Value.(string), nil
	}
	var lease bool
	if ls.mode == Never {
		lease = false
	} else if ls.mode == Always {
		lease = true
	} else if ls.mode == Normal {
		ls.wantCache <- key
		lease = <- ls.wantCacheReply
	}
	args := &storagerpc.GetArgs{Key: key, WantLease: lease, HostPort: ls.myHostPort}
	var reply storagerpc.GetReply
	cli, err := ls.getServer(key) 
	if  err != nil{
		return "", err
	}
	if err := cli.Call("StorageServer.Get", args, &reply); err != nil {
		return "", err
	}
	if reply.Status == storagerpc.OK {
		lease := reply.Lease
		if lease.Granted == true {
			elem := new(cache_elem)
			elem.Key = key
		    elem.ValidSeconds = lease.ValidSeconds
		    elem.Value = reply.Value
		    ls.addToCache <- elem
		    go ls.fireLease(elem)
		}
		return reply.Value, nil
	}
	return reply.Value, errors.New(string(reply.Status))
}

func (ls *libstore) Put(key, value string) error {
	args := &storagerpc.PutArgs{Key: key, Value: value}
	var reply storagerpc.PutReply
	cli, err := ls.getServer(key) 
	if  err != nil{
		return err
	}
	if err := cli.Call("StorageServer.Put", args, &reply); err != nil {
		return err
	}
	if reply.Status == storagerpc.OK {
		return nil
	}
	return errors.New(string(reply.Status))
}

func (ls *libstore) Delete(key string) error {
	args := &storagerpc.DeleteArgs{Key: key}
	var reply storagerpc.DeleteReply
	cli, err := ls.getServer(key) 
	if  err != nil{
		return err
	}
	if err := cli.Call("StorageServer.Delete", args, &reply); err != nil {
		return err
	}
	if reply.Status == storagerpc.OK {
		return nil
	}
	return errors.New(string(reply.Status))
}

func (ls *libstore) GetList(key string) ([]string, error) {
	ls.queryCache <- key
	ok := <- ls.queryCacheReply
	if ok {
		ls.getCache <- key
		v := <- ls.getCacheReply 
		return v.(*cache_elem).Value.([]string), nil
	}
	var lease bool
	if ls.mode == Never {
		lease = false
	} else if ls.mode == Always {
		lease = true
	} else if ls.mode == Normal {
		ls.wantCache <- key
		lease = <- ls.wantCacheReply
	}
	args := &storagerpc.GetArgs{Key: key, WantLease: lease, HostPort: ls.myHostPort}
	var reply storagerpc.GetListReply
	cli, err := ls.getServer(key) 
	if  err != nil{
		return nil, err
	}
	if err := cli.Call("StorageServer.GetList", args, &reply); err != nil {
		return nil, err
	}
	if reply.Status == storagerpc.OK {
		lease := reply.Lease
		if lease.Granted == true {
			elem := new(cache_elem)
			elem.Key = key
		    elem.ValidSeconds = lease.ValidSeconds
		    elem.Value = reply.Value
		    ls.addToCache <- elem
		    go ls.fireLease(elem)
		}
		return reply.Value, nil
	}

	return reply.Value, errors.New(string(reply.Status))
}

func (ls *libstore) RemoveFromList(key, removeItem string) error {
	args := &storagerpc.PutArgs{Key: key, Value: removeItem}
	var reply storagerpc.PutReply
	cli, err := ls.getServer(key) 
	if  err != nil{
		return err
	}
	if err := cli.Call("StorageServer.RemoveFromList", args, &reply); err != nil {
		return err
	}
	if reply.Status == storagerpc.OK {
		return nil
	}
	return errors.New(string(reply.Status))
}

func (ls *libstore) AppendToList(key, newItem string) error {
	if key == "invalidUser:sublist" {
	}
	args := &storagerpc.PutArgs{Key: key, Value: newItem}
	var reply storagerpc.PutReply
	cli, err := ls.getServer(key) 
	if  err != nil{
		return err
	}
	if err := cli.Call("StorageServer.AppendToList", args, &reply); err != nil {
		return err
	}
	if reply.Status == storagerpc.OK {
		return nil
	}
	return errors.New(string(reply.Status))
}

func (ls *libstore) RevokeLease(args *storagerpc.RevokeLeaseArgs, reply *storagerpc.RevokeLeaseReply) error {

	key := args.Key
	ls.goingToRevoke <- key
	ok := <- ls.gointToRevokeStatus
	if (!ok){
		reply.Status = storagerpc.KeyNotFound
	} else {
		reply.Status = storagerpc.OK
		ls.revokeKey <- key
	}
	return nil
}


type NodeList []storagerpc.Node

func (nodes NodeList) Len() int {
	return len (nodes)
}

func (nodes NodeList) Swap(i, j int) {
	nodes[i], nodes[j] = nodes[j], nodes[i]
}

func (nodes NodeList) Less (i, j int) bool {
	return nodes[i].NodeID < nodes[j].NodeID
}