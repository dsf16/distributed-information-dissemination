package storageserver

import (
    //"errors"
    //"fmt"
    "github.com/cmu440/tribbler/libstore"
    "github.com/cmu440/tribbler/rpc/storagerpc"
    "net"
    "net/http"
    "net/rpc"
    "strconv"
    "strings"
    "sync"
    "time"
    "sort"
)

type lease struct {
    key            string
    timeGranted    time.Time
    holderHostPort string
}

type dbValue struct {
    sync.RWMutex // lock on each key
    value interface{} // abstract value type
}

type storageServer struct {
    nodeID          uint32

    sync.Mutex // global lock on db
    db              map[string]*dbValue

    numNodes          int
    ring              NodeList
    ready      bool

    leases            map[string][]*lease
    getLeasesByKey    chan string
    leasesByKey       chan []*lease
    deleteLeasesByKey chan string
    deleteLease       chan *lease
    invalidKeys       []string
    isKeyValid      chan string
    keyIsValid      chan bool
    addInvalidKey     chan string
    keyIsNowValid     chan string
}

// NewStorageServer creates and starts a new StorageServer. masterServerHostPort
// is the master storage server's host:port address. If empty, then this server
// is the master; otherwise, this server is a slave. numNodes is the total number of
// servers in the ring. port is the port number that this server should listen on.
// nodeID is a random, unsigned 32-bit ID identifying this server.
//
// This function should return only once all storage servers have joined the ring,
// and should return a non-nil error if the storage server could not be started.
func NewStorageServer(masterServerHostPort string, numNodes, port int, nodeID uint32) (StorageServer, error) {
    // // fmt.Println("[SS] New Storage Server expecting numNodes:", numNodes)
    var args *storagerpc.RegisterArgs
    var reply *storagerpc.RegisterReply = new(storagerpc.RegisterReply)
    reply.Status = storagerpc.NotReady
    var n storagerpc.Node
    storageServer := &storageServer{
        nodeID:          nodeID,

        db:              make(map[string]*dbValue),

        numNodes:          numNodes,
        ring:              nil,
       
        ready:      false,

        leases:            make(map[string][]*lease),
        getLeasesByKey:    make(chan string),
        leasesByKey:       make(chan []*lease),
        deleteLeasesByKey: make(chan string),
        deleteLease:       make(chan *lease),
        invalidKeys:       nil,
        isKeyValid:      make(chan string),
        keyIsValid:      make(chan bool),
        addInvalidKey:     make(chan string),
        keyIsNowValid:     make(chan string),
    }
    if masterServerHostPort == "" { // If is master, register itself, return when all slaves are registered
        n = storagerpc.Node{HostPort: "localhost:"+strconv.Itoa(port), NodeID: nodeID}
        // fmt.Printf("******registering master server %v\n",n)
        // register itself
        // // fmt.Println("Have master register itself")
        storageServer.ring = append(storageServer.ring, n)
        rpc.RegisterName("MasterStorageServer", storagerpc.Wrap(storageServer))
        rpc.RegisterName("StorageServer", storagerpc.Wrap(storageServer))
        rpc.HandleHTTP()
        listener, err := net.Listen("tcp", "localhost:"+strconv.Itoa(port))
        if err != nil {
            return nil, err
        }
        go http.Serve(listener, nil)

        // wait until ready
        for (len(storageServer.ring) < numNodes){
            time.Sleep(time.Second)
        }
        go runLease(storageServer)
        return storageServer, nil

    } else { // If is slave, return when registerServer returns
        n = storagerpc.Node{HostPort: "localhost:"+strconv.Itoa(port), NodeID: nodeID}
        // fmt.Printf("******registering slave server %v\n",n)
        args = &storagerpc.RegisterArgs{ServerInfo: n}
        cli, err := rpc.DialHTTP("tcp", masterServerHostPort)
        if err != nil {
            return nil, err
        }
        
        cli.Call("MasterStorageServer.RegisterServer", args, &reply)
        tick := time.Tick(time.Duration(time.Second))
        for reply.Status != storagerpc.OK {
            cli.Call("MasterStorageServer.RegisterServer", args, &reply)
            <- tick
        }
        storageServer.ring = reply.Servers
        storageServer.ready = true
        err = rpc.RegisterName("StorageServer", storagerpc.Wrap(storageServer))
        if err != nil {

            return nil, err
        }
        rpc.HandleHTTP()
        listener, err := net.Listen("tcp", args.ServerInfo.HostPort)
        if err != nil {
            return nil, err
        }
        go http.Serve(listener, nil)
        go runLease(storageServer)
        return storageServer, nil

    }
}

func (ss *storageServer) RegisterServer(args *storagerpc.RegisterArgs, reply *storagerpc.RegisterReply) error {
    // reply: Servers, Status(OK, NotReady)
    node := args.ServerInfo

    exists := false
    before := ss.ring
    var after []storagerpc.Node
    for _, existingNode := range ss.ring {
        if existingNode.NodeID == node.NodeID {
            exists = true
            break        
        }
    }
    if !exists {
        newRing := append(before, node)
        ss.ring = append(newRing, after...)
    }     
    if len(ss.ring) == ss.numNodes {
        ss.ready = true
        reply.Status = storagerpc.OK
        sort.Sort(ss.ring)
        reply.Servers = ss.ring
    } else {
    	reply.Status = storagerpc.NotReady
    	reply.Servers = nil
    }
    
    return nil
}

func (ss *storageServer) GetServers(args *storagerpc.GetServersArgs, reply *storagerpc.GetServersReply) error {
    // // fmt.Println("[SS] GetServers", ss.nodeID)
    // reply: Server, Status(OK,NotReady)
    if ss.ready {
        reply.Status = storagerpc.OK
        reply.Servers = ss.ring
        // fmt.Println("GetServers reply:", reply.Servers)
    } else {
        reply.Status = storagerpc.NotReady
        reply.Servers = nil
        // fmt.Println("GetServers reply: NOT READY")
    }
    return nil
}


func runLease(ss *storageServer) {
    for {
        select {

        case key := <-ss.getLeasesByKey:
            ss.leasesByKey <- ss.leases[key]
        case key := <-ss.deleteLeasesByKey:
            delete(ss.leases, key)
        case deleteLease := <-ss.deleteLease:
            leases := ss.leases[deleteLease.key]
            for i, lease := range leases {
                if lease.holderHostPort == deleteLease.holderHostPort {
                    ss.leases[deleteLease.key] = append(leases[:i], leases[i+1:]...)
                    break
                }
            }
        case key := <- ss.isKeyValid:
            valid := true
            for _, invalidKey := range ss.invalidKeys {
                if key == invalidKey {
                    valid = false
                    break
                }
            }
            ss.keyIsValid <- valid
        case invalidKey := <-ss.addInvalidKey:
            ss.invalidKeys = append(ss.invalidKeys, invalidKey)
        case key := <-ss.keyIsNowValid:
            for i, invalidKey := range ss.invalidKeys {
                if key == invalidKey {
                    ss.invalidKeys = append(ss.invalidKeys[:i], ss.invalidKeys[i+1:]...)
                    break
                }
            }
        }
    }
}

func (ss *storageServer) Get(args *storagerpc.GetArgs, reply *storagerpc.GetReply) error {
    // fmt.Println("# [SS] Get from", ss.nodeID, args)
    // reply: Value, Lease if requested, Status(OK, WrongServer, KeyNotFound)
    ss.Lock()
    // // fmt.Println("[SS] [GLOBAL LOCK] Get", args, "has global lock")
    lValue, ok := ss.db[args.Key]
    if ok {
        lValue.RLock()
        // // fmt.Println("[SS] [KEY READ LOCK] Get", args, "has key read lock")
    }
    // // fmt.Println("[SS] [GLOBAL UNLOCK] Get", args)
    ss.Unlock()
    if !inServerRange(ss, libstore.StoreHash(args.Key)) {
        reply.Status = storagerpc.WrongServer
        reply.Value = ""
    } else if !ok {
        reply.Status = storagerpc.KeyNotFound
        reply.Value = ""
    } else { // ok
        reply.Status = storagerpc.OK
        reply.Value = lValue.value.(string)
        // // fmt.Println("[SS] [KEY READ UNLOCK] Get", args)
        lValue.RUnlock()
    }

    if args.WantLease {
        ss.isKeyValid <- args.Key
        reply.Lease = storagerpc.Lease{<-ss.keyIsValid, storagerpc.LeaseSeconds}
        existingLeases := ss.leases[args.Key]
        ss.leases[args.Key] = append(existingLeases, &lease{args.Key, time.Now(), args.HostPort})
    }
    // fmt.Println("[SS] Get reply for key", args.Key, "is", reply)
    return nil

}

func (ss *storageServer) Delete(args *storagerpc.DeleteArgs, reply *storagerpc.DeleteReply) error {
    // // fmt.Println("# [SS] Delete", args)
    // reply: Status(OK, WrongServer, KeyNotFound)
    ss.Lock()
    _, ok := ss.db[args.Key]
    if !inServerRange(ss, libstore.StoreHash(args.Key)) {
        reply.Status = storagerpc.WrongServer
    } else if !ok {
        reply.Status = storagerpc.KeyNotFound
    } else {
        reply.Status = storagerpc.OK
        delete(ss.db, args.Key)
    }
    ss.Unlock()
    return nil
}

func (ss *storageServer) GetList(args *storagerpc.GetArgs, reply *storagerpc.GetListReply) error {
    // // fmt.Println("# [SS] GetList", args)
    // reply: Value(list), Lease if requested, Status(OK, WrongServer, KeyNotFound)
    ss.Lock()
    lValue, ok := ss.db[args.Key]
    if ok {
        lValue.RLock()
    }
    ss.Unlock()
    if !inServerRange(ss, libstore.StoreHash(args.Key)) {
        reply.Status = storagerpc.WrongServer
    } else if !ok {
        reply.Status = storagerpc.KeyNotFound
    } else { // ok
        reply.Status = storagerpc.OK
        reply.Value = lValue.value.([]string)
        lValue.RUnlock()
    }
    if args.WantLease {
        ss.isKeyValid <- args.Key
        reply.Lease = storagerpc.Lease{<-ss.keyIsValid, storagerpc.LeaseSeconds}
        existingLeases := ss.leases[args.Key]
        ss.leases[args.Key] = append(existingLeases, &lease{args.Key, time.Now(), args.HostPort})
    }
    return nil
}

func (ss *storageServer) Put(args *storagerpc.PutArgs, reply *storagerpc.PutReply) error {
    // fmt.Println("# [SS] Put in", ss.nodeID, args)
    // reply: Status(OK, WrongServer)

    if !inServerRange(ss, libstore.StoreHash(args.Key)) {
        reply.Status = storagerpc.WrongServer
    } else {
        reply.Status = storagerpc.OK
    }
    ss.Lock()
    // // fmt.Println("[SS] [GLOBAL LOCK] Put", args, "has global lock")
    lValue, ok := ss.db[args.Key]
    if ok {
        lValue.Lock()
        // // fmt.Println("[SS] [KEY WRITE LOCK] Put", args, "has key write lock")
        // // fmt.Println("[SS] [GLOBAL UNLOCK] Put", args)
        ss.Unlock()
        handleLeases(ss, args.Key)
        lValue.value = args.Value
        // // fmt.Println("[SS] [KEY WRITE UNLOCK] Put", args)
        ss.keyIsNowValid <- args.Key
        lValue.Unlock()
    } else {
        ss.db[args.Key] = &dbValue{value: args.Value}
        // // fmt.Println("[SS] [GLOBAL UNLOCK] Put", args)
        ss.Unlock()
    }
    return nil
}

func (ss *storageServer) AppendToList(args *storagerpc.PutArgs, reply *storagerpc.PutReply) error {
    // // fmt.Println("# [SS] AppendToList", args)
    // reply: Status(OK, WrongServer, ItemExists)
    ss.Lock()
    lCurrentList, ok := ss.db[args.Key]
    keySplit := strings.Split(args.Key, ":")
    keyID := ""
    if len(keySplit) > 1 {
        keyID = keySplit[1]
    }
    if !inServerRange(ss, libstore.StoreHash(args.Key)) {
        ss.Unlock()
        reply.Status = storagerpc.WrongServer
    } else if !ok {
        ss.db[args.Key] = &dbValue{value: []string{args.Value}}
        ss.Unlock()
        reply.Status = storagerpc.OK
    } else { // ok
        lCurrentList.Lock()
        ss.Unlock()
        handleLeases(ss, args.Key)
        currentList := lCurrentList.value
        if keyID == "triblist" {
            //  string(later time) < string(earlier time)
            // maintain ordering from least to most recent
            before := currentList.([]string)
            var after []string
            itemExists := false
            for i, tribbleID := range currentList.([]string) {
                if args.Value < tribbleID {
                    if i == 0 {
                        before = nil
                        after = currentList.([]string)
                    } else {
                        before = currentList.([]string)[:i-1]
                        after = currentList.([]string)[i:]
                    }
                    break
                } else if args.Value == tribbleID {
                    itemExists = true
                    break
                }
            }
            if itemExists {
                reply.Status = storagerpc.ItemExists
            } else {
                reply.Status = storagerpc.OK
                newTribList := append(before, args.Value)
                lCurrentList.value = append(newTribList, after...)
            }
        } else {
            itemExists := false
            for _, listItem := range currentList.([]string) {
                if args.Value == listItem {
                    itemExists = true
                    break
                }
            }
            if itemExists {
                reply.Status = storagerpc.ItemExists
            } else {
                reply.Status = storagerpc.OK
                lCurrentList.value = append(currentList.([]string), args.Value)
            }
        }
        ss.keyIsNowValid <- args.Key
        lCurrentList.Unlock()
    }
    return nil
}

func (ss *storageServer) RemoveFromList(args *storagerpc.PutArgs, reply *storagerpc.PutReply) error {
    // // fmt.Println("# [SS] RemoveFromList", args)
    // reply: Status(OK, WrongServer, ItemNotFound)
    ss.Lock()
    lCurrentList, ok := ss.db[args.Key]
    reply.Status = storagerpc.ItemNotFound
    if !inServerRange(ss, libstore.StoreHash(args.Key)) {
        ss.Unlock()
        reply.Status = storagerpc.WrongServer
    } else if ok {
        lCurrentList.Lock()
        ss.Unlock()
        handleLeases(ss, args.Key)
        currentList := lCurrentList.value
        for i, val := range currentList.([]string) {
            if val == args.Value {
                reply.Status = storagerpc.OK
                lCurrentList.value = append(currentList.([]string)[:i], currentList.([]string)[i+1:]...)
                break
            }
        }
        ss.keyIsNowValid <- args.Key
        lCurrentList.Unlock()
    }
    return nil
}

func inServerRange(ss *storageServer, hash uint32) bool {
    servers := ss.ring
    var correctNodeID uint32
    if servers != nil && len(servers) > 0 {
        correctNodeID = servers[0].NodeID // always at least one server
    } else {
        correctNodeID = 0
    }
    
    for _,server := range servers {
        if server.NodeID >= hash {
            correctNodeID = server.NodeID
            break
        }
    }
    return (correctNodeID == ss.nodeID)
}

func handleLeases(ss *storageServer, key string) {
    // // fmt.Println("[SS] HANDLE LEASES FOR KEY:", key)
    ss.addInvalidKey <- key
    ss.getLeasesByKey <- key
    leases := <-ss.leasesByKey
    if leases == nil || len(leases) == 0 {
        return
    }
    
    latest := leases[len(leases)-1].timeGranted
    validDuration := time.Duration(storagerpc.LeaseSeconds + storagerpc.LeaseGuardSeconds)
    expiryTime := latest.Add(validDuration*time.Second)
    args := &storagerpc.RevokeLeaseArgs{Key: key}

    // block until either all lease holders have replied
    // or all the leases have expired (+ guard time)
    // send RevokeLease RPC
    for _, lease := range leases {
        if time.Now().After(expiryTime) {
            ss.deleteLeasesByKey <- key
            // // fmt.Println("[SS] deleted lease from storage bc of expiry", args)
            break
        }
        go revokeLease(ss, args, lease) // TODO: new go routine for each rpc call or for the whole loop?
    }

    for leases != nil && len(leases) > 0 {
        if time.Now().After(expiryTime) {
            ss.deleteLeasesByKey <- key
            // // fmt.Println("[SS] deleted lease from storage bc of expiry", args)
            break
        }

        ss.getLeasesByKey <- key
        leases = <-ss.leasesByKey
    }
}

func revokeLease(ss *storageServer, args *storagerpc.RevokeLeaseArgs, lease *lease) {
    cli, err := rpc.DialHTTP("tcp", lease.holderHostPort)
    if err != nil {
        return
    }
    var reply storagerpc.RevokeLeaseReply
    if err := cli.Call("LeaseCallbacks.RevokeLease", args, &reply); err != nil {
        return
    }
    // // fmt.Println("[SS] revoked lease", args)
    // delete lease from list
    ss.deleteLease <- lease
}


type NodeList []storagerpc.Node

func (nodes NodeList) Len() int {
	return len (nodes)
}

func (nodes NodeList) Swap(i, j int) {
	nodes[i], nodes[j] = nodes[j], nodes[i]
}

func (nodes NodeList) Less (i, j int) bool {
	return nodes[i].NodeID < nodes[j].NodeID
}
